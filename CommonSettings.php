<?php

# Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

## Include platform/distribution defaults
require_once "$IP/includes/PlatformSettings.php";

$wgScriptPath = "/w";

## The URL path to static resources (images, scripts, etc.)
$wgResourceBasePath = $wgScriptPath;

## The URL paths to the logo.  Make sure you change this from the default,
## or else you'll overwrite your logo when you upgrade!
$wgLogos = [ '1x' => "$wgResourceBasePath/resources/assets/wiki.png" ];

# Shared database table
# This has no effect unless $wgSharedDB is also set.
$wgSharedTables[] = "actor";

# HACK for test framework initialization issue
global $wgCanonicalNamespaceNames;
$wgCanonicalNamespaceNames = NamespaceInfo::CANONICAL_NAMES;

###########################################################
## Old school stuff above, new shiny stuff below
###########################################################
$wgBootstrap->setConfigDir( __DIR__ );
$wgBootstrap->set( 'MainCacheType', CACHE_ACCEL );

$wgBootstrap->loadSettings( 'settings.json' );
$wgBootstrap->loadSettings( 'datacenter.json' );
$wgBootstrap->loadSettings( 'servergroup.json' );
$wgBootstrap->loadBootstrapConfig( 'config/secrets.json' );

if ( $wgCommandLineMode ) {
	$wgBootstrap->loadSettings( 'cli.json' );
}

if ( method_exists( $wgBootstrap, 'loadSiteSettings' ) ) {
	$wgBootstrap->setWikiFarmSiteSettings( 'sites' );
	$wgBootstrap->loadSiteSettings();
} else {
	$wgBootstrap->loadSettings( 'sites/localhost.json' );
}

$wmfDatacenter = $wgBootstrap->get( 'WmfDataCenter' );
$wmfRealm = $wgBootstrap->get( 'WmfRealm' );
$wmfAllServices = $wgBootstrap->get( 'WmfAllServices' );
$wmfServices = $wmfAllServices[$wmfDatacenter];

// pretend this comes from etcd: $etcdConfig = wmfSetupEtcd( $wmfLocalServices['etcd'] );
$etcdConfigData = json_decode( file_get_contents( __DIR__ . '/config/etcd.json ' ), true );
$etcdConfig = new HashConfig( $etcdConfigData );

$wgBootstrap->set( 'ReadOnly', $etcdConfig->get( "$wmfDatacenter/ReadOnly" ) );

// FIXME: inject this in a bunch of places
$wmfMasterDatacenter = $etcdConfig->get( 'common/WMFMasterDatacenter' );
// $wgBootstrap->set( 'WmfEtcdLastModifiedIndex', $etcdConfig->getModifiedIndex() );

if ( $wmfMasterDatacenter !== $wmfDatacenter ) {
	// FIXME: is this even used for anything?
	$wmfMasterServices = $wmfAllServices[$wmfMasterDatacenter];
} else {
	$wmfMasterServices = $wmfServices;
}

// FIXME: compe up with a better way to keep this out of the globals.
$wgBootstrap->delete( 'WmfAllServices' );

$wgBootstrap->intercept( 'LockManagers', function( $lockManagers ) use ( $wmfMasterServices, $wgBootstrap ) {
	$lockManagers['lockServers'] = $wmfMasterServices['redis_lock']; // depends on master DB
	$lockManagers['redisConfig']['password'] = $wgBootstrap->get( 'RedisPassword' ); // from secrets
	return $lockManagers;
} );

if ( $wmfRealm === 'production' ) { // XXX: see wmfEtcdApplyDBConfig()
	$wmfDbconfigFromEtcd = $etcdConfig->get( "$wmfDatacenter/dbconfig" );
	$wgBootstrap->intercept( 'LBFactoryConf', function( $lbFactoryConf ) use ( $wmfDbconfigFromEtcd ) {
		$lbFactoryConf['readOnlyBySection'] = $wmfDbconfigFromEtcd['readOnlyBySection'];
		$lbFactoryConf['groupLoadsBySection'] = $wmfDbconfigFromEtcd['groupLoadsBySection'];
		$lbFactoryConf['hostsByName'] = $wmfDbconfigFromEtcd['hostsByName'];
		foreach ( $wmfDbconfigFromEtcd['sectionLoads'] as $section => $dbctlLoads ) {
			// For each section, MediaWiki treats the first host as the master.
			// Since JSON dictionaries are unordered, dbctl stores an array of two host:load
			// dictionaries, one containing the master and one containing all the replicas.
			$loadByHost = array_merge( $dbctlLoads[0], $dbctlLoads[1] );
			$lbFactoryConf['sectionLoads'][$section] = $loadByHost;
		}
		// Since MediaWiki components that use ExternalStore includes cluster names in the rows
		// of blob tracking tables, the periodic consolidation of clusters by DBAs requires the
		// preservation of cluster aliases in order to handle all the old cluster references.
		$externalStoreAliasesByCluster = [
			# es1, previously known as $wmgOldExtTemplate
			'es1' => [ 'rc1', 'cluster3', 'cluster4', 'cluster5', 'cluster6', 'cluster7',
				'cluster8', 'cluster9', 'cluster10', 'cluster20', 'cluster21',
				'cluster1', 'cluster2', 'cluster22', 'cluster23' ],
			'es2' => [ 'cluster24' ],
			'es3' => [ 'cluster25' ],
			'es4' => [ 'cluster26' ],
			'es5' => [ 'cluster27' ],
			'x1' => [ 'extension1' ],
			'x2' => [ 'extension2' ],
		];
		foreach ( $wmfDbconfigFromEtcd['externalLoads'] as $dbctlCluster => $dbctlLoads ) {
			// For each external cluster, MediaWiki treats the first host as the master.
			// Since JSON dictionaries are unordered, dbctl stores an array of two host:load
			// dictionaries, one containing the master and one containing all the replicas.
			$loadByHost = array_merge( $dbctlLoads[0], $dbctlLoads[1] );
			foreach ( $externalStoreAliasesByCluster[$dbctlCluster] as $mwLoadName ) {
				$lbFactoryConf['externalLoads'][$mwLoadName] = $loadByHost;
			}
		}
		return $lbFactoryConf;
	} );
}

/*
$mv = MWMultiVersion::initializeForWiki( $_SERVER['SERVER_NAME'] );
$site = $mv->getDatabase();
if ( !$wgBootstrap->loadSiteSettings( $site ) ) {
	include __DIR__ . '/missing.php';
}
*/
